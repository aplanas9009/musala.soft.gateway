package soft.musala.gateway.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import soft.musala.gateway.model.Gateway;

import java.util.List;
import java.util.Optional;

@Repository
public interface GatewayRepository extends CrudRepository<Gateway, String> {

    Optional<Gateway> findById(String s);

    List<Gateway> findAll();

    Optional<Gateway> findByNameAndAddress(String name, String address);
}
