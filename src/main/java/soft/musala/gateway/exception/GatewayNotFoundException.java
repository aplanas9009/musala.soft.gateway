package soft.musala.gateway.exception;

import javassist.NotFoundException;
import soft.musala.gateway.model.Gateway;

public class GatewayNotFoundException extends NotFoundException {
    public GatewayNotFoundException(Class<Gateway> gatewayClass) {
        super(String.valueOf(Gateway.class));
    }
}
