package soft.musala.gateway.exception;

public class AlreadyExistException extends RuntimeException {
    public AlreadyExistException(String exceptionDetail) {
        super(exceptionDetail);
    }
}
