package soft.musala.gateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import soft.musala.gateway.exception.GatewayNotFoundException;
import soft.musala.gateway.model.Gateway;
import soft.musala.gateway.model.dto.GatewayDTO;
import soft.musala.gateway.service.GatewayService;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static java.lang.Math.abs;
import static java.util.Objects.nonNull;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static soft.musala.gateway.utils.HttpUtil.*;

@RestController
@RequestMapping(value = "/gateway")
public class GatewayController {

    private final GatewayService gatewayService;

    @Autowired
    public GatewayController(GatewayService gatewayService) {
        this.gatewayService = gatewayService;
    }

    @GetMapping
    public List<GatewayDTO> getAllGateway() {
        return gatewayService.showAllGateways().stream()
                .map(GatewayDTO::from)
                .collect(Collectors.toList());
    }

    @GetMapping(value = "/{serial}")
    public GatewayDTO getGatewayById(@PathVariable String serial) throws GatewayNotFoundException {
        return GatewayDTO
                .from(
                        gatewayService.showGatewayBySerial(serial)
                                .orElseThrow(() -> new GatewayNotFoundException(Gateway.class))
                );
    }

    @DeleteMapping(value = "/{serial}")
    public ResponseEntity deleteBaseWorkById(@PathVariable String serial) {
        gatewayService.deleteGateway(serial);
        return gatewayService.showGatewayBySerial(serial).isPresent()
                ? requestErrorStatus(BAD_REQUEST, Gateway.class)
                : requestOkStatus(RESPONSE_OK_BODY);
    }

    @PatchMapping(value = "/{serial}")
    public ResponseEntity updateGateway(@PathVariable String serial, @Valid @RequestBody GatewayDTO gatewayDTO) {

        gatewayService.showGatewayBySerial(serial).ifPresent(gateway1 -> {
            if (gateway1.getDevices().size() + gatewayDTO.getDevices().size() > 10)
                gatewayDTO.removePeripherals(abs(gateway1.getDevices().size() - gatewayDTO.getDevices().size()));
        });

        Gateway gateway = gatewayService.updateGateway(serial, gatewayDTO);
        return nonNull(gateway)
                ? requestOkStatus(GatewayDTO.from(gateway))
                : requestErrorStatus(BAD_REQUEST, Gateway.class);
    }

    @PostMapping
    public ResponseEntity saveGateway(@Valid @RequestBody GatewayDTO gatewayDTO) {
        if(gatewayDTO.getDevices().size() > 10)
            gatewayDTO.removePeripherals(abs(gatewayDTO.getDevices().size() - 10));

        Gateway saveGateway = gatewayService.addGateway(gatewayDTO);
        return requestOkStatus(GatewayDTO.from(saveGateway));
    }

}
