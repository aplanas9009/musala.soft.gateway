package soft.musala.gateway.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import soft.musala.gateway.exception.AlreadyExistException;
import soft.musala.gateway.model.Gateway;
import soft.musala.gateway.model.dto.GatewayDTO;
import soft.musala.gateway.repository.GatewayRepository;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class GatewayServiceImpl implements GatewayService {

    private final GatewayRepository gatewayRepository;

    @Override
    public Optional<Gateway> showGatewayBySerial(String serial) {
        return gatewayRepository.findById(serial);
    }

    @Override
    public List<Gateway> showAllGateways() {
        return gatewayRepository.findAll();
    }

    @Override
    public Gateway addGateway(GatewayDTO gatewayDTO) {
        if (!gatewayRepository.findByNameAndAddress(gatewayDTO.getName(), gatewayDTO.getAddress()).isPresent())
            throw new AlreadyExistException("Gateway " + gatewayDTO.getName() + "already exist in ip: " + gatewayDTO.getAddress());

        return gatewayRepository.save(
                new Gateway(
                        gatewayDTO.getName(),
                        gatewayDTO.getAddress(),
                        gatewayDTO.getDevices()
                )
        );
    }

    @Override
    public Gateway updateGateway(String serial, GatewayDTO gatewayDTO) {
        return gatewayRepository.findById(serial)
                .map(gateway -> {
                    gateway.setName(gatewayDTO.getName());
                    gateway.setAddress(gatewayDTO.getAddress());
                    gateway.setDevices(gatewayDTO.getDevices());
                    return gatewayRepository.save(gateway);
                }).orElse(null);
    }

    @Override
    public void deleteGateway(String serial) {
        gatewayRepository.deleteById(serial);
    }
}
