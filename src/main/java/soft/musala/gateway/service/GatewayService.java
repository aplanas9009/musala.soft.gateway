package soft.musala.gateway.service;

import soft.musala.gateway.model.Gateway;
import soft.musala.gateway.model.dto.GatewayDTO;

import java.util.List;
import java.util.Optional;

public interface GatewayService {

    Optional<Gateway> showGatewayBySerial(String serial);

    List<Gateway> showAllGateways();

    Gateway addGateway(GatewayDTO gateway);

    Gateway updateGateway(String serial, GatewayDTO gateway);

    void deleteGateway(String serial);

}
