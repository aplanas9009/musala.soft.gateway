package soft.musala.gateway.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class HttpUtil {
    public static final Map<String, String> RESPONSE_OK_BODY = Collections.singletonMap("status", "success");
    static final Map<String, String> RESPONSE_ERROR_BODY = new HashMap<>();

    static {
        RESPONSE_ERROR_BODY.put("status", "error");
    }

    private HttpUtil() {
    }

    public static <T> ResponseEntity<T> requestErrorStatus(HttpStatus status, T responseBody) {
        return ResponseEntity.status(status).body(responseBody);
    }

    public static <T> ResponseEntity<T> requestOkStatus(T entity) {
        return ResponseEntity.ok().body(entity);
    }
}
