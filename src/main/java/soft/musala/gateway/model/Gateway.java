package soft.musala.gateway.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "gateway")
@EqualsAndHashCode(callSuper = false)
public class Gateway {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String serial;

    @Column
    private String name;

    @Valid
    @Column
    private String address;

    @OneToMany(mappedBy = "gateway", cascade = CascadeType.ALL)
    private List<Peripheral> devices;

    public Gateway(String name, String address, List<Peripheral> devices) {
        this.name = name;
        this.address = address;
        this.devices = devices;
    }
}
