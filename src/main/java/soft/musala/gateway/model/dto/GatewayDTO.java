package soft.musala.gateway.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import soft.musala.gateway.model.Gateway;
import soft.musala.gateway.model.Peripheral;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class GatewayDTO {

    private String name;
    @NotNull
    @Pattern(regexp = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$")
    private String address;
    private List<Peripheral> devices;

    public static GatewayDTO from(Gateway gateway) {
        return new GatewayDTO(
                gateway.getName(),
                gateway.getAddress(),
                gateway.getDevices()
        );
    }

    public void removePeripherals(int number){
        while (number > 0) {
            devices.remove(devices.size() - 1);
            number--;
        }
    }

}
