package soft.musala.gateway.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "peripheral")
public class Peripheral {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int uid;

    @Column
    private String vendor;

    @Column
    private Date created;

    @Column
    private Status status;

    @ManyToOne
    @JoinColumn(name = "serial")
    private Gateway gateway;

}
