package soft.musala.gateway.model;

public enum Status {

    ONLINE("online"),
    OFFLINE("offline");

    private final String status;

    Status(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }
}
