package soft.musala.gateway.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import soft.musala.gateway.model.Gateway;
import soft.musala.gateway.model.dto.GatewayDTO;
import soft.musala.gateway.repository.GatewayRepository;
import soft.musala.gateway.service.GatewayService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@WebMvcTest(GatewayController.class)
public class GatewayControllerTest {

    @Autowired
    GatewayRepository gatewayRepository;

    @Autowired
    GatewayService gatewayService;

    @Autowired
    private MockMvc mvc;

    private JacksonTester<List<GatewayDTO>> jsonGateListJacksonTester;
    private JacksonTester<GatewayDTO> jsonGateJacksonTester;


    @Before
    public void setup() {
        JacksonTester.initFields(this, new ObjectMapper());
        gatewayRepository.saveAll(
                Arrays.asList(
                        new Gateway("g1", "10.10.10.1", Collections.emptyList()),
                        new Gateway("g2", "10.10.10.2", Collections.emptyList()),
                        new Gateway("g3", "10.10.10.3", Collections.emptyList())
                )
        );
    }

    @After
    public void tearDown() {
        gatewayRepository.deleteAll();
    }


    @Test
    public void getAllGateway() throws Exception {
        MockHttpServletResponse response = mvc.perform(get("/gateway")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        GatewayDTO gatewayDTO = GatewayDTO.from(gatewayRepository.findAll().get(0));
        GatewayDTO gatewayDTO1 = GatewayDTO.from(gatewayRepository.findAll().get(1));
        GatewayDTO gatewayDTO2 = GatewayDTO.from(gatewayRepository.findAll().get(2));

        assertThat(jsonGateListJacksonTester.parse(response.getContentAsString()).getObject())
                .contains(gatewayDTO, gatewayDTO1, gatewayDTO2);
    }

    @Test
    public void deleteBaseWorkById() throws Exception {
        Gateway gateway = gatewayRepository.findAll().get(2);
        MockHttpServletResponse response = mvc.perform(delete("/gateway/" + gateway.getSerial())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertThat(jsonGateListJacksonTester.parse(response.getContentAsString()).getObject())
                .doesNotContain(GatewayDTO.from(gateway));
    }

    @Test
    public void updateGateway() throws Exception {
        Gateway gateway = gatewayRepository.findAll().get(2);
        gateway.setName("G22");
        MockHttpServletResponse response = mvc.perform(patch("/gateway/" + gateway.getSerial())
                .content(new ObjectMapper()
                        .writeValueAsString(GatewayDTO.from(gateway)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();

        assertThat(jsonGateJacksonTester.parse(response.getContentAsString()).getObject().getName())
                .isEqualTo("G22");
    }

    @Test
    public void saveGateway() throws Exception {
        Gateway gateway = new Gateway("new", "10.10.10.4", Collections.emptyList());
        MockHttpServletResponse response = mvc.perform(post("/gateway")
                .content(new ObjectMapper()
                        .writeValueAsString(GatewayDTO.from(gateway)))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse();
        assertThat(jsonGateJacksonTester.parse(response.getContentAsString()).getObject())
                .extracting("name", "address")
                .contains("new", "10.10.10.4");
    }
}