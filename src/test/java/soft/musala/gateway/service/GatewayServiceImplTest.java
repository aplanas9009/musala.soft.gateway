package soft.musala.gateway.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import soft.musala.gateway.model.Gateway;
import soft.musala.gateway.model.dto.GatewayDTO;
import soft.musala.gateway.repository.GatewayRepository;

import java.util.Arrays;

import static java.util.Collections.emptyList;
import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class GatewayServiceImplTest {

    @Mock
    GatewayRepository gatewayRepository;

    @InjectMocks
    GatewayServiceImpl gatewayService;

    @Test
    public void showGatewayBySerial() {
        Gateway gateway = new Gateway("G1", "1.1.1.1", emptyList());
        gateway.setSerial("fakeId");
        when(gatewayRepository.findById("fakeId")).thenReturn(of(gateway));
        assertThat(gatewayService.showGatewayBySerial("fakeId").get())
                .extracting("name", "address")
                .contains("G1", "1.1.1.1");
    }

    @Test
    public void showAllGateways() {
        Gateway gateway = new Gateway("G1", "1.1.1.1", emptyList());
        Gateway gateway1 = new Gateway("G2", "1.1.1.2", emptyList());
        when(gatewayRepository.findAll()).thenReturn(Arrays.asList(gateway, gateway1));
        assertThat(gatewayService.showAllGateways())
                .contains(gateway, gateway1);
    }

    @Test
    public void addGateway() {
        Gateway gateway = new Gateway("G1", "1.1.1.1", emptyList());
        gateway.setSerial("fakeId");
        when(gatewayRepository.save(gateway)).thenReturn(gateway);
        assertThat(gatewayService.addGateway(GatewayDTO.from(gateway)))
                .extracting("name", "address")
                .contains("G1", "1.1.1.1");
    }

    @Test
    public void updateGateway() {
        Gateway gateway = new Gateway("G1", "1.1.1.1", emptyList());
        gateway.setSerial("fakeId");
        when(gatewayRepository.save(gateway)).thenReturn(gateway);
        assertThat(gatewayService.updateGateway("fakeId", GatewayDTO.from(new Gateway("G", "2.2.2.2", emptyList()))))
                .extracting("name", "address")
                .contains("G", "2.2.2.2");
    }
}